core = 7.x
api = 2


; DEFAULTS =====================================================================

defaults[projects][subdir] = contrib


; MODULES ======================================================================

; System
projects[ml_access][type] = module
projects[ml_access][subdir] = system
projects[ml_access][download][type] = git
projects[ml_access][download][url] = git@bitbucket.org:minima-labs/ml-access.git
projects[ml_access][download][tag] = 7.x-1.0

projects[ml_admin][type] = module
projects[ml_admin][subdir] = system
projects[ml_admin][download][type] = git
projects[ml_admin][download][url] = git@bitbucket.org:minima-labs/ml-admin.git
projects[ml_admin][download][tag] = 7.x-1.0

projects[ml_cookies][type] = module
projects[ml_cookies][subdir] = system
projects[ml_cookies][download][type] = git
projects[ml_cookies][download][url] = git@bitbucket.org:minima-labs/ml-cookies.git
projects[ml_cookies][download][tag] = 7.x-2.0

projects[ml_core][type] = module
projects[ml_core][subdir] = system
projects[ml_core][download][type] = git
projects[ml_core][download][url] = git@bitbucket.org:minima-labs/ml-core.git
projects[ml_core][download][tag] = 7.x-2.0

projects[ml_machine_name][type] = module
projects[ml_machine_name][subdir] = system
projects[ml_machine_name][download][type] = git
projects[ml_machine_name][download][url] = git@bitbucket.org:minima-labs/ml-machine-name.git
projects[ml_machine_name][download][tag] = 7.x-1.0

projects[ml_mail][type] = module
projects[ml_mail][subdir] = system
projects[ml_mail][download][type] = git
projects[ml_mail][download][url] = git@bitbucket.org:minima-labs/ml-mail.git
projects[ml_mail][download][tag] = 7.x-2.0

projects[ml_pages][type] = module
projects[ml_pages][subdir] = system
projects[ml_pages][download][type] = git
projects[ml_pages][download][url] = git@bitbucket.org:minima-labs/ml-pages.git
projects[ml_pages][download][tag] = 7.x-1.0

projects[ml_panels][type] = module
projects[ml_panels][subdir] = system
projects[ml_panels][download][type] = git
projects[ml_panels][download][url] = git@bitbucket.org:minima-labs/ml-panels.git
projects[ml_panels][download][tag] = 7.x-2.0

projects[ml_wysiwyg][type] = module
projects[ml_wysiwyg][subdir] = system
projects[ml_wysiwyg][download][type] = git
projects[ml_wysiwyg][download][url] = git@bitbucket.org:minima-labs/ml-wysiwyg.git
projects[ml_wysiwyg][download][tag] = 7.x-2.0

; Contrib
projects[addressfield][version] = 1.0-beta5
projects[addressfield_tokens][version] = 1.3
projects[admin_menu][version] = 3.0-rc4
projects[advagg][version] = 2.6
projects[better_formats][version] = 1.0-beta1
projects[cdn][version] = 2.6
projects[ctools][version] = 1.4
projects[date][version] = 2.7
projects[devel][version] = 1.4
projects[diff][version] = 3.2
projects[domain][version] = 3.11
projects[draggableviews][version] = 2.0
projects[elements][version] = 1.4
projects[email][version] = 1.2
projects[email_registration][version] = 1.1
projects[emogrifier][version] = 1.18
projects[emogrifier][patch][1842946] = http://drupal.org/files/emogrifier-1842946-8.patch
projects[entity][version] = 1.5
projects[entityreference][version] = 1.1
projects[features][version] = 2.0
projects[field_group][version] = 1.3
projects[file_entity][version] = 2.0-alpha3
projects[file_entity][patch][2192391] = http://drupal.org/files/issues/file_entity_remove_file_display-2192391-01.patch
projects[geocoder][version] = 1.2
projects[geofield][version] = 2.1
projects[geophp][version] = 1.7
projects[globalredirect][version] = 1.5
projects[google_analytics][version] = 1.4
projects[htmlmail][version] = 2.65
projects[jquery_update][version] = 2.x-dev
projects[less][version] = 4.x-dev
projects[less][patch][2295881] = http://drupal.org/files/issues/less-use---modify-var-argument-for-lessjs-2295881-1.patch
projects[libraries][version] = 2.2
projects[link][version] = 1.2
projects[linkit][version] = 3.1
projects[mailsystem][version] = 2.34
projects[media][version] = 2.0-alpha3
projects[media][patch][2104193] = http://drupal.org/files/issues/media_remove_file_display_alter-2104193-23.patch
projects[menu_admin_per_menu][version] = 1.0
projects[menu_block][version] = 2.3
projects[metatag][version] = 1.0-beta9
projects[migrate][version] = 2.6-rc1
projects[migrate_extras][version] = 2.5
projects[minima_config][type] = module
projects[minima_config][download][type] = git
projects[minima_config][download][url] = https://bitbucket.org/minima-labs/minima_config.git
projects[minima_config][download][branch] = 7.x-1.x
projects[module_filter][version] = 2.0-alpha2
projects[office_hours][version] = 1.3
projects[panelizer][version] = 3.1
projects[panels][version] = 3.4
projects[panels_everywhere][version] = 1.0-rc1
projects[pathauto][version] = 1.2
projects[pathologic][version] = 2.12
projects[redirect][version] = 1.0-rc1
projects[reroute_email][version] = 1.1
projects[role_delegation][version] = 1.1
projects[rules][version] = 2.6
projects[site_map][version] = 1.0
projects[smtp][version] = 1.0
projects[strongarm][version] = 2.0
projects[token][version] = 1.5
projects[transliteration][version] = 3.1
projects[views][version] = 3.7
projects[views_bulk_operations][version] = 3.2
projects[webform][version] = 3.20
projects[webform_validation][version] = 1.4
projects[wysiwyg][version] = 2.x-dev
projects[wysiwyg_filter][version] = 1.6-rc2
projects[xmlsitemap][version] = 2.0-rc2


; THEMES =======================================================================

projects[minima][type] = theme
projects[minima][subdir] = system
projects[minima][download][type] = git
projects[minima][download][url] = http://git.drupal.org/sandbox/ollynevard/1886822.git
projects[minima][download][branch] = 7.x-2.x


; LIBRARIES ====================================================================

projects[ckeditor][type] = library
projects[ckeditor][download][type] = git
projects[ckeditor][download][url] = https://github.com/ckeditor/ckeditor-releases.git
projects[ckeditor][download][tag] = 4.3.1/full
projects[ckeditor][subdir] = ""

projects[cycle2][type] = library
projects[cycle2][download][type] = file
projects[cycle2][download][url] = http://malsup.github.com/min/jquery.cycle2.min.js
projects[cycle2][subdir] = ""

projects[cycle2.swipe][type] = library
projects[cycle2.swipe][download][type] = file
projects[cycle2.swipe][download][url] = http://malsup.github.io/min/jquery.cycle2.swipe.min.js
projects[cycle2.swipe][subdir] = ""

projects[emogrifier_library][type] = library
projects[emogrifier_library][directory_name] = emogrifier
projects[emogrifier_library][download][type] = git
projects[emogrifier_library][download][url] = https://github.com/jjriv/emogrifier.git
projects[emogrifier_library][download][revision] = 82457befc8695a895f62c895da4614815c613112
projects[emogrifier_library][subdir] = ""

projects[jquery-placeholder][type] = library
projects[jquery-placeholder][download][type] = git
projects[jquery-placeholder][download][url] = https://github.com/mathiasbynens/jquery-placeholder.git
projects[jquery-placeholder][download][tag] = v2.0.7
projects[jquery-placeholder][subdir] = ""

projects[minima_library][type] = library
projects[minima_library][directory_name] = minima
projects[minima_library][download][type] = git
projects[minima_library][download][url] = https://github.com/nievo/minima.git
projects[minima_library][download][branch] = master
projects[minima_library][subdir] = ""

projects[selecter][type] = library
projects[selecter][download][type] = git
projects[selecter][download][url] = https://github.com/benplum/Selecter.git
projects[selecter][download][tag] = 3.0.0
projects[selecter][subdir] = ""

projects[selectivizr][type] = library
projects[selectivizr][download][type] = git
projects[selectivizr][download][url] = https://github.com/keithclark/selectivizr.git
projects[selectivizr][download][tag] = 1.0.2
projects[selectivizr][subdir] = ""
